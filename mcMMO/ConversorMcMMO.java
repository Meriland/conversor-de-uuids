import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ConversorMcMMO {

    private static final Object fileWritingLock = new Object();
    
    private final static String usersFilePath = "C:\\Users\\Administrador\\Documents\\Dani\\MeriY\\plugins\\mcMMO\\flatfile\\mcmmo.users";
    
    public static void main(String[] args) {
        new ConversorMcMMO(); 
    }
    private ConversorMcMMO() {
        List<String> usuarios = getStoredUsers();
        if (usuarios.isEmpty()) System.out.println("Archivo vacio, erroneo o no encontrado!");
        usuarios.stream().forEach((nombre) -> {
            try {
                saveUserUUID(nombre);
                System.out.println(nombre + " convertido");
            } catch (Exception ex) {
                System.out.println("No se ha convertido "+nombre);
            }
        });
        System.out.println("Conversión terminada!");
    } 
    
    
    private List<String> getStoredUsers() {
        ArrayList<String> users = new ArrayList<>();
        BufferedReader in = null;

        synchronized (fileWritingLock) {
            try {
                // Open the user file
                in = new BufferedReader(new FileReader(usersFilePath));
                String line;

                while ((line = in.readLine()) != null) {
                    String[] character = line.split(":");
                    users.add(character[0]);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {}
                }
            }
        }
        return users;
    }
    
    private boolean saveUserUUID(String userName) {
        boolean worked = false;

        BufferedReader in = null;
        FileWriter out = null;

        synchronized (fileWritingLock) {
            try {
                in = new BufferedReader(new FileReader(usersFilePath));
                StringBuilder writer = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {
                    String[] character = line.split(":");
                    if (!worked && character[0].equalsIgnoreCase(userName)) {
                        if (character.length < 42) {
                            System.out.println("No se ha podido encontrar la UUID para " + userName + "!");
                            System.out.println("La entrada de la base de datos es erronea.");
                            break;
                        }

                        line = line.replace(character[41], "");
                        line = line.substring(0, line.length() - (character[42].length() + 2)); //Hay que sacrificar D:
                        worked = true;
                    }

                    writer.append(line).append("\r\n");
                }

                out = new FileWriter(usersFilePath); // Write out the new file
                out.write(writer.toString());
            } catch (Exception e) {
                System.out.println("Ha ocurrido un error analizando '" + usersFilePath + "' (¿Estás en el directorio correcto?)");
                System.out.println(e.getMessage());
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {}
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {}
                }
            }
        }

        return worked;
    }
    
}
