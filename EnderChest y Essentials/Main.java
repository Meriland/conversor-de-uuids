package es.meriland.uuid;

import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin implements Listener {

    private static final Charset UTF8 = Charset.forName("UTF-8");
    
    @Override
    public void onEnable() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(this, this);
    }
    
    @Override
    public void onDisable() {}
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("conversor")) {
            if (sender.hasPermission("Meriland.conversor")) {
                switch (args.length) {
                    case 2:
                        UUID nuevaUUID = UUID.fromString(args[1]);
                        UUID viejaUUID = offlineID(args[0]);
                        if (convertir(viejaUUID, nuevaUUID)) {
                            sender.sendMessage(ChatColor.AQUA + "Datos de '"+args[0]+"' convertidos correctamente al nombre de '"+args[1]+"'");
                        } else {
                            sender.sendMessage(ChatColor.RED + "Ha ocurrido un error trasformando los datos de '"+args[0]+"' a '"+args[1]+"'!!!!");
                        }
                        break;
                    case 3:
                        UUID InuevaUUID = UUID.fromString(args[1]);
                        UUID IviejaUUID = offlineID(args[0]);
                        String opc = args[2];
                        
                        if (opc.equalsIgnoreCase("-enderchest")) {
                            if (Iconvertir(IviejaUUID, InuevaUUID, 1)) {
                                sender.sendMessage(ChatColor.AQUA + "Datos de '"+args[0]+"' convertidos correctamente al nombre de '"+args[1]+"'"+ChatColor.GREEN+" Enderchest excluido");
                            } else {
                                sender.sendMessage(ChatColor.RED + "Ha ocurrido un error trasformando los datos de '"+args[0]+"' a '"+args[1]+"'!!!!");
                            }
                        } else if (opc.equalsIgnoreCase("-essentials")) {
                            if (Iconvertir(IviejaUUID, InuevaUUID, 2)) {
                                sender.sendMessage(ChatColor.AQUA + "Datos de '"+args[0]+"' convertidos correctamente al nombre de '"+args[1]+"'"+ChatColor.GREEN+" Essentials excluido");
                            } else {
                                sender.sendMessage(ChatColor.RED + "Ha ocurrido un error trasformando los datos de '"+args[0]+"' a '"+args[1]+"'!!!!");
                            }
                        }
                        break;
                    default:
                        sender.sendMessage(ChatColor.GREEN+"Usa /conversor <viejoNombre> <nuevoUUID> [-enderchest | -essentials]");
                        break;
                }
            } else {
                sender.sendMessage(ChatColor.RED+"No tienes permiso!");
            }
        }
        return true;
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLogin(PlayerLoginEvent event) {
        UUID offlineId;
        UUID onlineId;
        String p = event.getPlayer().getName();
        
            /* Registrar UUIDs del jugador */
        offlineId = offlineID(p);
        onlineId = event.getPlayer().getUniqueId();

        convertir(offlineId, onlineId);
    }
    
    private boolean convertir(UUID offlineId, UUID onlineId) {
        return Iconvertir(offlineId, onlineId, -1);
    }
    
    private boolean Iconvertir(UUID offlineId, UUID onlineId, int i) {
        File chestData = new File(this.getDataFolder().getParentFile().getAbsolutePath() + File.separator + "BetterEnderChest" + File.separator + "chestData" + File.separator);
        File chestDataSurvival = new File(chestData.getAbsolutePath() + File.separator + "survival");
        File essData = new File(this.getDataFolder().getParentFile().getAbsolutePath() + File.separator + "Essentials" + File.separator + "userdata");
        
        File datosViejos1 = new File(chestData, offlineId.toString()+".dat");
        File nuevosDatos1 = new File(chestData, onlineId.toString() +".dat");
                                
        File datosViejos2 = new File(chestDataSurvival, offlineId.toString()+".dat");
        File nuevosDatos2 = new File(chestDataSurvival, onlineId.toString() + ".dat");
        
        File datosViejos3 = new File(essData, offlineId.toString()+".yml");
        File nuevosDatos3 = new File(essData, onlineId.toString() +".yml");
        
        try {
            if (datosViejos1.exists() && i != 1) moverArchivo("BEC",datosViejos1, nuevosDatos1);
            if (datosViejos2.exists() && i != 1) moverArchivo("BEC-SURVIVAL",datosViejos2, nuevosDatos2);
            if (datosViejos3.exists() && i != 2) moverArchivo("ESS",datosViejos3, nuevosDatos3);
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "Error al mover los archivos. IOException. {0}", ex.getMessage());
            return false;
        } catch (Throwable t) {
            getLogger().log(Level.SEVERE, "Error no esperado al mover los archivos: {0}", t.getMessage());
            return false;
        }
        
        return true;
    }
    
    private void moverArchivo(String pre, File oldFile, File newFile) throws IOException {
        if (!oldFile.renameTo(newFile)) {
            // Try copy and delete
            getLogger().log(Level.FINE, "[{0}]Fallo al renombrar el archivo {1}, probando a copiar y eliminar", new Object[]{pre, oldFile.getAbsolutePath()});
            Files.copy(oldFile, newFile);
            if (!oldFile.delete()) {
                getLogger().log(Level.WARNING, "[{0}]Fallo al eliminar el antiguo archivo {1} tras copiarlo a {2}", new Object[]{pre, oldFile.getAbsolutePath(), newFile.getAbsolutePath()});
            }
        } else getLogger().log(Level.INFO, "[{0}]Archivo {1} movido a {2}", new Object[]{pre, oldFile.getName(), newFile.getName()});
    }
    
    private UUID offlineID(String nombre) {
        return java.util.UUID.nameUUIDFromBytes( ( "OfflinePlayer:"+ nombre).getBytes(UTF8) );
    }
}
